//
//  EditeMessageView.m
//  MultiPoster
//
//  Created by Admin on 18.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "EditeMessageView.h"

static CGFloat const ViewContenMargin = 8.0f;
static CGFloat const ViewContentOffset = 8.0f;

@implementation EditeMessageView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ImagePaperPlane"]];
        
        _iconButton = [[UIButton alloc] init];
        _iconButton.backgroundColor = [UIColor lightGrayColor];
        _iconButton.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_iconButton];
        
        _MessageMemo = [[UITextView alloc] init];
        _MessageMemo.layer.borderWidth = 2.0f;
        _MessageMemo.layer.borderColor = [[UIColor blackColor] CGColor];
        _MessageMemo.layer.cornerRadius = 5.0f;
        _MessageMemo.clipsToBounds      = YES;
        [self addSubview:_MessageMemo];
        [_MessageMemo becomeFirstResponder];
        
        _contentInsets = UIEdgeInsetsMake(ViewContenMargin,
                                          ViewContenMargin,
                                          ViewContenMargin,
                                          ViewContenMargin);
        
        _removePictureButton = [[UIButton alloc] init];
        _removePictureButton.backgroundColor = [UIColor clearColor];
        _removePictureButton.contentMode = UIViewContentModeScaleAspectFit;
        [_removePictureButton setBackgroundImage: [UIImage imageNamed:@"ImageRemove"] forState:UIControlStateNormal];
        [self addSubview:_removePictureButton];
        [_removePictureButton setHidden:YES];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    CGFloat shrinkIndex = 0.6f;
    CGFloat topOffset = (1.2f-shrinkIndex)/2.0f*actualBounds.size.width;
    
    
    _iconButton.frame = CGRectMake(actualBounds.origin.x + ((1.0f-shrinkIndex)/2.0f*actualBounds.size.width),
                                   topOffset + ViewContentOffset,
                                   actualBounds.size.width*shrinkIndex,
                                   actualBounds.size.width*shrinkIndex);
    

    //_iconButton.layer.borderWidth = 2.0f;
    _iconButton.layer.cornerRadius = 5.0f;
    _iconButton.backgroundColor = [UIColor grayColor];
    
    CGFloat removePictureIconSize = 45.0f;
    
    _removePictureButton.frame = CGRectMake(_iconButton.frame.origin.x+_iconButton.frame.size.width, _iconButton.frame.origin.y-removePictureIconSize, removePictureIconSize, removePictureIconSize);
    
    
    _MessageMemo.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    _MessageMemo.frame = CGRectMake(actualBounds.origin.x,
                                                 _iconButton.frame.origin.y + _iconButton.frame.size.height + 2.0f*ViewContentOffset,
                                                 self.frame.size.width - 2.0f*ViewContentOffset,
                                    self.frame.size.height - (_iconButton.frame.origin.y + _iconButton.frame.size.height + 2.0f*ViewContentOffset)-ViewContentOffset-(self.keyboardHeight?self.keyboardHeight:0));
}



@end
