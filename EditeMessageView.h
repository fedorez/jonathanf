//
//  EditeMessageView.h
//  MultiPoster
//
//  Created by Admin on 18.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditeMessageView : UIView
@property (nonatomic, strong) UIButton *iconButton;
@property (nonatomic, strong) UIButton *removePictureButton;
@property (nonatomic, strong) UITextView *MessageMemo;

@property (nonatomic, assign) UIEdgeInsets contentInsets;
@property (nonatomic, assign) CGFloat keyboardHeight;



@end
