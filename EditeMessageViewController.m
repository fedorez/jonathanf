//
//  EditeMessageViewController.m
//  MultiPoster
//
//  Created by Admin on 18.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "EditeMessageViewController.h"
#import "EditeMessageView.h"
#import <Photos/Photos.h>
#import "VKService.h"
#import "FacebookService.h"
#import "Session.h"

@interface EditeMessageViewController ()<UITextFieldDelegate, UITextViewDelegate,  UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic, strong) EditeMessageView *EditView;
@property (nonatomic, strong) NSData *imageData;
@end

@implementation EditeMessageViewController

- (void)loadView {
    [super loadView];
    self.EditView = [[EditeMessageView alloc] init];
    [self setView:self.EditView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(sendItem:)];
    [self.EditView.iconButton addTarget:self action:@selector(editImage:) forControlEvents:UIControlEventTouchUpInside ];
    [self.EditView.removePictureButton addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside ];
    
    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWillBeShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGRect kb = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.EditView.keyboardHeight = kb.size.height;
    [self.EditView layoutSubviews];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    self.EditView.keyboardHeight = 0;
    [self.EditView layoutSubviews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendItem:(id)sender {
    if ([[VKService sharedService] online]) {
        if ([[VKService sharedService] isLoggedIn]) {
                [[VKService sharedService] getUrlwithparams:self.imageData andText:self.EditView.MessageMemo.text];
        }
    }
    if ([[FacebookService sharedService] online]) {
        if ([[FacebookService sharedService] isLoggedIn]) {
                [[FacebookService sharedService] sendUrlwithparams:self.imageData andText:self.EditView.MessageMemo.text];
        }
    }
    
    //NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
    //[userInfo setObject:[NSNumber numberWithInt:1] forKey:@"total"];
    
    //NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    //[nc postNotificationName:@"EditMessageVCPopped" object:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)editImage:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    UIPopoverPresentationController *presentationController = picker.popoverPresentationController;
    //presentationController.barButtonItem = button;  // display popover from the UIBarButtonItem as an anchor
    presentationController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)deleteImage:(id)sender {
    [self.EditView.iconButton setBackgroundImage:nil forState:UIControlStateNormal] ;
    self.imageData = nil;
    [self.EditView.removePictureButton setHidden:YES];
}


-(void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo:(nonnull NSDictionary<NSString *,id> *)info{
    //UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage] ;
    [self.EditView.iconButton setBackgroundImage:[info objectForKey:UIImagePickerControllerEditedImage] forState:UIControlStateNormal ] ;
    self.imageData = UIImageJPEGRepresentation([info objectForKey:UIImagePickerControllerEditedImage], 1.0f);
    [self.EditView.removePictureButton setHidden:NO];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    picker = nil;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self reloadInputViews];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
