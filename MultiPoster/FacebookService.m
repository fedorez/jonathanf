//
//  Service.m
//  MultiPoster
//
//  Created by Denis Fedorets on 20.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "FacebookService.h"
#import "Session.h"
#import "Reachability.h"

NSString *FacebookServiceReachibilityChangedNotification = @"FacebookServiceReachibilityChangedNotification";

@interface FacebookService ()

@property (nonatomic, strong) NSURLSession *facebookDataSession;
@property (nonatomic, strong) NSURLSession *imagesDownloadSession;

@property (nonatomic, strong) Session *session;

@property (nonatomic, strong) NSMutableDictionary *imagesDowndloadTasks;

@property (nonatomic, strong) Reachability *reachability;

@end

@implementation FacebookService

+ (instancetype)sharedService {
    static FacebookService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[FacebookService alloc] init];
    });
    return sharedService;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _imagesDowndloadTasks = [[NSMutableDictionary alloc] init];
        
        _session = [[Session alloc] init];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _facebookDataSession = [NSURLSession sessionWithConfiguration:configuration];
        
        _reachability = [Reachability reachabilityForInternetConnection];
        [_reachability startNotifier];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChangedNotification:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
    }
    return self;
}

- (void)loginWithToken:(NSString *)token {
    self.session.token = token;
}

- (BOOL)isLoggedIn {
    return self.session.token.length > 0;
}

- (void)logoutWithCompletion:(FacebookServiceCompletion)completion {
    self.session.token = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        
        NSError *errors;
        
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        
        if (completion) {
            completion(YES, nil);
        }
    });
}

-(void) sendUrlwithparams:(NSData*)image andText:(NSString*)text{
    
    NSString* getWallUploadServer = [NSString stringWithFormat:@"https://graph.facebook.com/me/photos?access_token=%@",self.session.token];
    
    [self sendPOSTRequest:getWallUploadServer withImageData:image andAccessToken:self.session.token andUserId:self.session.UserId andText:text];
    
}

- (void) sendPOSTRequest:(NSString *)reqURl withImageData:(NSData *)imageData andAccessToken:(NSString*)accessToken andUserId:(NSString*)userId andText:(NSString*)text{
    NSLog(@"Sending request: %@", reqURl);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:reqURl]];
    [request setHTTPMethod:@"POST"];
    
    NSString *formBoundary = [NSString stringWithFormat:@"%@.boundary.%08x%08x", @"asdasdasdasd", arc4random(), arc4random()];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", formBoundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"photo.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"image/jpg"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:imageData]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    [[_facebookDataSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *photoId = [json objectForKey:@"id"];
        
        [self sendText:accessToken andPhotoId:photoId andText:text];
        
    }] resume];
}

-(void) sendText:(NSString*)accessToken andPhotoId:(NSString*)photoId andText:(NSString*)text {
    //NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    // Создаем запрос на размещение текста на стене
    NSString *sendTextMessage = [NSString stringWithFormat: @"https://graph.facebook.com/me/feed?message=%@&object_attachment=%@&access_token=%@", text, photoId, accessToken];
    NSLog(@"sendTextMessage: %@", sendTextMessage);
    
    
    NSURL *url = [NSURL URLWithString:sendTextMessage];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    [[_facebookDataSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"get%lu", (unsigned long)json.count);
        NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
        [userInfo setObject:[NSNumber numberWithInt:1] forKey:@"total"];
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"NewMessageArrived" object:self userInfo:userInfo];
    }] resume];
}

- (NSString *)URLEncodedString:(NSString *)str
{
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                             (CFStringRef)str,
                                                                                             NULL,
                                                                                             CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                             kCFStringEncodingUTF8));
    
    return result;
}

- (void)loadMediaWithCompletion:(FacebookServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/me/feed?fields=id,message,updated_time,full_picture,shares,likes.summary(true).limit(0)&access_token=%@", self.session.token]];
    
    [[_facebookDataSession dataTaskWithURL:url
                         completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 if (completion) {
                                     completion(!error, data, error);
                                 }
                             });
                         }] resume];
}

- (NSString *)loadDataWithURL:(NSString *)urlString completion:(FacebookServiceDataCompletion)completion {
    NSString *UUID = [NSUUID UUID].UUIDString;
    
    NSURLSessionDataTask *task = [_facebookDataSession dataTaskWithURL:[NSURL URLWithString:urlString]
                                                     completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             if (completion) {
                                                                 completion(!error, data, error);
                                                             }
                                                         });
                                                         [self cancelTaskWithUUID:UUID];
                                                     }];
    self.imagesDowndloadTasks[UUID] = task;
    [task resume];
    return UUID;
}

- (void) logOff
{
    //self.session.token = [self.session.userDefaults removeobjectForKey:SessionTokenUserDefaultsKey];
    [self.session.userDefaults removeObjectForKey:@"SessionTokenUserDefaultsKey"];
    [self.session.userDefaults synchronize];
}

- (void)cancelTaskWithUUID:(NSString *)UUID {
    NSURLSessionDataTask *task = self.imagesDowndloadTasks[UUID];
    [task cancel];
    [self.imagesDowndloadTasks removeObjectForKey:UUID];
}

- (BOOL)online {
    return [self.reachability currentReachabilityStatus] != NotReachable;
}

#pragma mark - Private methods

- (void)reachabilityChangedNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] postNotificationName:FacebookServiceReachibilityChangedNotification object:nil];
}

@end

