//
//  MessagesModel.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "MessagesModel.h"
#import "AppDelegate.h"

#import "Message.h"
#import "Pagination.h"
#import "Mapper.h"
#import "FacebookService.h"
#import "VKService.h"

@interface MessagesModel ()

@property (nonatomic, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, strong) NSArray *fetchedPhotos;

@property (nonatomic, strong) Pagination *paginationFB;
@property (nonatomic, strong) Pagination *paginationVK;
@property (nonatomic, strong) NSNumber* offset;

@end

@implementation MessagesModel

- (instancetype)init {
    self = [super init];
    if (self) {
        _persistentContainer = [((AppDelegate *)[UIApplication sharedApplication].delegate) persistentContainer];
    }
    return self;
}

- (NSInteger)numberOfRows {
    return self.fetchedPhotos.count;
}

- (Message *)itemForRow:(NSInteger)row {
        return [self.fetchedPhotos objectAtIndex:row];
}

- (void)reloadDataWithCompletion:(void (^)())completion {
    if ([[VKService sharedService] isLoggedIn]) {
        if ([[VKService sharedService] online]) {
            [[VKService sharedService] loadMediaWithCompletion:^(BOOL success, NSData *data, NSError *error) {
                if (success) {
                    NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(mergeChangesFromContextDidSaveNotification:)
                                                                 name:NSManagedObjectContextDidSaveNotification
                                                               object:context];
                    [context performBlock:^{
                        Pagination *pagination =  [Mapper objectsOfClass:[Pagination class]
                                                          withJSONData:data
                                                          keypath:@"response"
                                                          forSourceType:@"vk"].firstObject;
                        self.offset = [NSNumber numberWithInt:0];
                    
                        NSArray *messages = [Mapper entitiesOfClass:[Message class]
                                                    withJSONData:data
                                                    keypath:@"response.items"
                                                    inContext:context
                                                    forSourceType:@"vk"];
                        for (NSInteger index = 0; index < [messages count]; index++) {
                            Message *currMsg = messages[index];
                            currMsg.source_type = @"vk";
                        }
                    
                        [self removeDeletedPhotosWithLoadedPhotos:messages inContext:context];
                    
                        NSError *error = nil;
                        [context save:&error];
                        if (error) {
                            NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                        }
                        [[NSNotificationCenter defaultCenter] removeObserver:self];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.paginationVK = pagination;
                            [self refetchData];
                        
                            if (completion) {
                                completion();
                            }
                        });
                    }];
                }
                else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self refetchData];
                            if (completion) {
                                completion();
                            }
                        });
                }
            }];
        }
        else {
            self.paginationVK = nil;
            [self refetchData];
            if (completion) {
                completion();
            }
        }
    }
    
    if ([[FacebookService sharedService] online]) {
        if ([[FacebookService sharedService] isLoggedIn]) {
            [[FacebookService sharedService] loadMediaWithCompletion:^(BOOL success, NSData *data, NSError *error) {
                if (success) {
                    NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                             selector:@selector(mergeChangesFromContextDidSaveNotification:)
                                                                 name:NSManagedObjectContextDidSaveNotification
                                                               object:context];
                    [context performBlock:^{
                        Pagination *pagination =  [Mapper objectsOfClass:[Pagination class]
                                                          withJSONData:data
                                                          keypath:@"paging"
                                                          forSourceType:@"facebook"].firstObject;
                        
                        
                        
                        
                        NSArray *messages = [Mapper entitiesOfClass:[Message class]
                                                    withJSONData:data
                                                    keypath:@"data"
                                                    inContext:context
                                                    forSourceType:@"facebook"];
                        for (NSInteger index = 0; index < [messages count]; index++) {
                            Message *currMsg = messages[index];
                            currMsg.source_type = @"facebook";
                        }
                        
                        [self removeDeletedPhotosWithLoadedPhotos:messages inContext:context];
                        
                        NSError *error = nil;
                        [context save:&error];
                        if (error) {
                            NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                        }
                        [[NSNotificationCenter defaultCenter] removeObserver:self];

                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.paginationFB = pagination;
                            [self refetchData];
                            
                            if (completion) {
                                completion();
                            }
                        });
                    }];
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self refetchData];
                        if (completion) {
                            completion();
                        }
                    });
                }
            }];
        }
        else {
            self.paginationFB = nil;
            [self refetchData];
            if (completion) {
                completion();
            }
        }
    }
}

- (BOOL)canLoadNextPart: (NSString *) sourceType {
    if ([sourceType isEqualToString:@"VK"]) {
        return self.paginationVK &&( ([self.offset intValue] + 3)< [self.paginationVK.nextMaxId intValue] );
    }
    if ([sourceType isEqualToString:@"facebook"]) {
        return self.paginationFB && self.paginationFB.nextURL.length > 0;
    }
    return NO;
}

- (void)loadNextPartWithCompletion:(void (^)())completion {
    if ([[VKService sharedService] online]) {
        if ([[VKService sharedService] isLoggedIn]) {
            if ([self canLoadNextPart:@"VK"]) {
                int value = [self.offset intValue];
                self.offset = [NSNumber numberWithInt:value + 3];
                 NSString *url = @"";
                if ( [self.offset intValue]  > ([self.paginationVK.nextMaxId intValue] - [self.offset intValue]))
                {
                    url = [NSString stringWithFormat:@"https://api.vk.com/method/wall.get?count=%d&offset=%@&access_token=%@&v=5.60",[self.paginationVK.nextMaxId intValue] - [self.offset intValue] , [self.offset stringValue], [[VKService sharedService] at]];
                }
                else {
                    
                       url = [NSString stringWithFormat:@"https://api.vk.com/method/wall.get?count=3&offset=%@&access_token=%@&v=5.60",[self.offset stringValue], [[VKService sharedService] at]];
                      }
                
                [[VKService sharedService] loadDataWithURL:url completion:^(BOOL success, NSData *data, NSError *error) {
                    if (success) {
                        NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                    
                        [context performBlock:^{
                            Pagination *pagination =  [Mapper objectsOfClass:[Pagination class]
                                                              withJSONData:data
                                                              keypath:@"response"
                                                              forSourceType:@"vk"].firstObject;
                        
                            NSArray *messages = [Mapper entitiesOfClass:[Message class]
                                                        withJSONData:data
                                                        keypath:@"response.items"
                                                        inContext:context
                                                        forSourceType:@"vk"];
                            for (NSInteger index = 0; index < [messages count]; index++) {
                                Message *currMsg = messages[index];
                                currMsg.source_type = @"vk";
                            }
                            
                            [self removeDeletedPhotosWithLoadedPhotos:messages inContext:context];
                        
                            NSError *error = nil;
                            [context save:&error];
                            if (error) {
                                NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                            }
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.paginationVK = pagination;
                                [self refetchData];
                            
                                if (completion) {
                                    completion();
                                }
                            });
                        }];
                    }
                }];
            }
        }
        else {
            self.paginationVK = nil;
            [self refetchData];
            if (completion) {
                completion();
            }
        }
    }
    
    if ([[FacebookService sharedService] online]) {
        if ([[FacebookService sharedService] online]) {
            if ([self canLoadNextPart:@"facebook"]) {
                [[FacebookService sharedService] loadDataWithURL:self.paginationFB.nextURL completion:^(BOOL success, NSData *data, NSError *error) {
                    if (success) {
                        NSManagedObjectContext *context = [self.persistentContainer newBackgroundContext];
                        
                        [context performBlock:^{
                            Pagination *pagination =  [Mapper objectsOfClass:[Pagination class]
                                                                        withJSONData:data
                                                                        keypath:@"paging"
                                                                        forSourceType:@"facebook"].firstObject;
                            
                            NSArray *messages = [Mapper entitiesOfClass:[Message class]
                                                                        withJSONData:data
                                                                        keypath:@"data"
                                                                        inContext:context
                                                                        forSourceType:@"facebook"];
                            for (NSInteger index = 0; index < [messages count]; index++) {
                                Message *currMsg = messages[index];
                                currMsg.source_type = @"facebook";
                            }
                            
                            [self removeDeletedPhotosWithLoadedPhotos:messages inContext:context];
                            
                            NSError *error = nil;
                            [context save:&error];
                            if (error) {
                                NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
                            }
                            dispatch_async(dispatch_get_main_queue(), ^{
                                self.paginationFB = pagination;
                                [self refetchData];
                                
                                if (completion) {
                                    completion();
                                }
                            });
                        }];
                    }
                }];
            }
        }
        else {
            self.paginationFB = nil;
            [self refetchData];
            if (completion) {
                completion();
            }
        }
    }
}

#pragma mark - Private methods

- (void)refetchData {
    NSFetchRequest *photosRequest = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
    photosRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"message_dt" ascending:NO]];
    
    if ([[VKService sharedService] online]) {
        if ([[VKService sharedService] isLoggedIn]) {
            if (!self.paginationVK) {
                return;
            }
        
                    }
    }
    
    if ([[FacebookService sharedService] online]) {
        if ([[FacebookService sharedService] isLoggedIn]) {
            if (!self.paginationFB) {
                return;
            }
            
           // if (self.paginationFB.nextPhotoId) {
               // photosRequest.predicate = [NSPredicate predicateWithFormat:@"messageId <= %@", self.paginationFB.nextPhotoId];
           // }
        }
    }
    
    NSError *error = nil;
    self.fetchedPhotos = [self.persistentContainer.viewContext executeFetchRequest:photosRequest
                                                                             error:&error];
    //[self.fetchedPhotos sortedArrayUsingComparator^NSComparisonResult(, id  _Nonnull obj2) {
       // <#code#>
    //}];
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
    }
    
}

- (void)removeDeletedPhotosWithLoadedPhotos:(NSArray *)loadedPhotos inContext:(NSManagedObjectContext *)context {
    NSNumber *maxId = [loadedPhotos valueForKeyPath:@"@max.messageId"];
    NSNumber *minId = [loadedPhotos valueForKeyPath:@"@min.messageId"];
    
    NSPredicate *deletedPredicate = [NSPredicate predicateWithFormat:@"(messageId >= %@) AND (messageId <= %@) AND NOT (messageId  IN %@)", minId, maxId, [loadedPhotos valueForKey:@"messageId"]];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
    fetchRequest.predicate = deletedPredicate;
    
    NSError *error = nil;
    NSArray *deletedPhotos = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.description);
    }
    
    for (NSManagedObject *entity in deletedPhotos) {
        [context deleteObject:entity];
    }
}

- (void)mergeChangesFromContextDidSaveNotification:(NSNotification *)notification {
    [self.persistentContainer.viewContext performBlock:^{
        [self.persistentContainer.viewContext mergeChangesFromContextDidSaveNotification:notification];
    }];
}


@end
