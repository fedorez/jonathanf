//
//  Session.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Session.h"

static NSString *SessionTokenUserDefaultsKey = @"SessionTokenUserDefaultsKey";
static NSString *SessionTokenUserDefaultsKeyVK = @"SessionTokenUserDefaultsKeyVK";
static NSString *SessionUserDefaultsUserVK = @"SessionUserDefaultsUserVK";

@interface Session ()



@end

@implementation Session

- (instancetype)init {
    self = [super init];
    if (self) {
        _userDefaults = [NSUserDefaults standardUserDefaults];
        _token = [_userDefaults objectForKey:SessionTokenUserDefaultsKey];
        _tokenVK = [_userDefaults objectForKey:SessionTokenUserDefaultsKeyVK];
        _UserId = [_userDefaults objectForKey:SessionUserDefaultsUserVK];
    }
    return self;
}

- (void)setToken:(NSString *)token {
    if (![_token isEqualToString:token]) {
        _token = token;
        [self.userDefaults setObject:token forKey:SessionTokenUserDefaultsKey];
        [self.userDefaults synchronize];
    }
}

    - (void)setTokenVK:(NSString *)token {
        if (![_tokenVK isEqualToString:token]) {
            _tokenVK = token;
            // _UserId = UserId;
            // [self.userDefaults setObject:UserId forKey:SessionUserDefaultsUserVK];
            [self.userDefaults setObject:token forKey:SessionTokenUserDefaultsKeyVK];
            [self.userDefaults synchronize];
        }
   
   /* if (![_tokenVK isEqualToString:token]) {
        _tokenVK = nil;//token;
        [self.userDefaults setObject:token forKey:SessionTokenUserDefaultsKeyVK];
        [self.userDefaults synchronize];
    }*/
}

- (void)setUserId:(NSString *)UserId {
    if (![_UserId isEqualToString:UserId]) {
        _UserId = UserId;
        [self.userDefaults setObject:UserId forKey:SessionUserDefaultsUserVK];
        
        [self.userDefaults synchronize];
    }}


@end
