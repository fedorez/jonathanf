//
//  PushAnimator.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "PushAnimator.h"

@implementation PushAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIView *container = [transitionContext containerView];
    
    UIView *fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    toView.frame = container.bounds;
    
    CGAffineTransform defaultTransform = CGAffineTransformMakeTranslation(container.bounds.size.width, 0.0f);
    toView.transform = defaultTransform;
    [container addSubview:toView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         toView.transform = CGAffineTransformIdentity;
                         fromView.transform = CGAffineTransformInvert(defaultTransform);
                     } completion:^(BOOL finished) {
                         [fromView removeFromSuperview];
                         fromView.transform = CGAffineTransformIdentity;
                         [transitionContext completeTransition:finished];
                     }];
}

@end
