//
//  MessagesTableViewCell.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Message;

@interface MessagesTableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;
+ (CGFloat)heightForItem:(id)item widht:(CGFloat)widht;
+ (CGFloat)heightForItem:(id)item widht:(CGFloat)widht withImage:(UIImage *)image;

- (instancetype)init;
- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;

- (void)setItem:(Message *)item;

@end
