//
//  VKService.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString *VKServiceReachibilityChangedNotification;

typedef void (^VKServiceCompletion) (BOOL success, NSError *error);
typedef void (^VKServiceDataCompletion) (BOOL success, NSData *data, NSError *error);

@interface VKService : NSObject
@property (nonatomic, strong) NSString * at;
+ (instancetype)sharedService;
- (BOOL)online;


- (void)loginWithToken:(NSString *)token andUserId:(NSString*)UserId;

- (BOOL)isLoggedIn;

- (void)logoutWithCompletion:(VKServiceCompletion)completion;

- (void)loadMediaWithCompletion:(VKServiceDataCompletion)completion;

- (NSString *)loadDataWithURL:(NSString *)urlString completion:(VKServiceDataCompletion)completion;

- (void)cancelTaskWithUUID:(NSString *)UUID;
-(void) getUrlwithparams:(NSData*)image andText:(NSString*)text;
- (void)logOff;


@end
