//
//  FacebookLoginViewController.h
//  MultiPoster
//
//  Created by Denis Fedorets on 20.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FacebookLoginViewControllerDelegate <NSObject>

- (void)didLoadWebContent;
- (void)didRecieveToken:(NSString *)token;

@end

@interface FacebookLoginViewController : UIViewController

@property (nonatomic, weak) id<FacebookLoginViewControllerDelegate> delegate;

- (void)loadWebConttent;

@end
