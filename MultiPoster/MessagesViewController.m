//
//  MessagesViewController.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessagesTableViewCell.h"

#import "FacebookService.h"
#import "VKService.h"
#import "VKLoginViewController.h"
#import "FacebookLoginViewController.h"
#import "AppDelegate.h"
#import "MessagesModel.h"
#import "EditeMessageViewController.h"
#import "SVPullToRefresh.h"
#import "LoginViewController.h"


@interface MessagesViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, MessagesModelDelegate,  FacebookLoginViewControllerDelegate, VKLoginViewControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) MessagesModel *model;
@property (nonatomic, strong) FacebookLoginViewController *facebookLoginController;
@property (nonatomic, strong) VKLoginViewController *vkLoginController;

@end

@implementation MessagesViewController

- (void)loadView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.view = self.tableView;
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ImagePaperPlane"]];}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak MessagesViewController *weakSelf = self;
    
    // setup pull-to-refresh
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf reloadMessagesByPullGesture];
    }];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recvData:)
                                                 name:@"NewMessageArrived"
                                               object:nil];
    
    self.model = [[MessagesModel alloc] init];
    self.model.delegate = self;
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.hidesBackButton = YES;

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddMessage:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showLogoffPanel:)];
    
    [self.model reloadDataWithCompletion:^{
        [self.tableView reloadData];
    }];
}


- (void) showLogoffPanel:(id)sender {
    
    if ((![[VKService sharedService] online])&&(![[FacebookService sharedService] online])) {
        return;
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"Accounts" message:@"Select action:" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* actionLogInFacebook = [UIAlertAction actionWithTitle:@"Log In Facebook" style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          self.facebookLoginController= [[FacebookLoginViewController alloc] init];
                                                          self.facebookLoginController.delegate = self;
                                                          [self.facebookLoginController loadWebConttent];
                                                      }];
    UIImage *imageFB=[UIImage imageNamed:@"facebookLogoForNotificatoins"];
    [actionLogInFacebook setValue:[imageFB imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    
    
    UIAlertAction* actionLogOffFacebook = [UIAlertAction actionWithTitle:@"Log Off Facebook" style:UIAlertActionStyleDestructive
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          [[FacebookService sharedService] logOff];
                                                          [[FacebookService sharedService] logoutWithCompletion:^(BOOL success, NSError *error) {
                                                              if ((![[FacebookService sharedService] isLoggedIn]) && (![[VKService sharedService] isLoggedIn]))
                                                              {
                                                                  LoginViewController *LController = [[LoginViewController alloc] init];
                                                                  [self.navigationController pushViewController:LController animated:YES];
                                                              }
                                                              
                                                          }];
                                                      }];
    [actionLogOffFacebook setValue:[imageFB imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    
    
    
    UIImage *imageVK=[UIImage imageNamed:@"vkLogoForNotifications"];
    UIAlertAction* actionLogInVK = [UIAlertAction actionWithTitle:@"Log In VK" style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    self.vkLoginController = [[VKLoginViewController alloc] init];
                                                                    self.vkLoginController.delegate = self;
                                                                    [self.vkLoginController loadWebContent];
                                                                }];
    [actionLogInVK setValue:[imageVK imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    UIAlertAction* actionLogOffVK = [UIAlertAction actionWithTitle:@"Log Off VK" style:UIAlertActionStyleDestructive
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     [[VKService sharedService] logOff];
                                                                     [[VKService sharedService] logoutWithCompletion:^(BOOL success, NSError *error) {
                                                                         if ((![[FacebookService sharedService] isLoggedIn]) && (![[VKService sharedService] isLoggedIn]))
                                                                         {
                                                                             LoginViewController *LController = [[LoginViewController alloc] init];
                                                                             [self.navigationController pushViewController:LController animated:YES];
                                                                         }
                                                                         
                                                                     }];
                                                                 }];

    [actionLogOffVK setValue:[imageVK imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    
    
    UIAlertAction* actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             NSLog(@"Cancel action selected");
                                                         }];
    
    if ([[FacebookService sharedService] isLoggedIn]) {
        [alert addAction:actionLogOffFacebook];
    }
    else {
        [alert addAction:actionLogInFacebook];
    }
    if ([[VKService sharedService] isLoggedIn]) {
        [alert addAction:actionLogOffVK];
    }
    else {
        [alert addAction:actionLogInVK];
    }
    [alert addAction:actionCancel];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)didLoadWebContent {
    [self.navigationController pushViewController:self.facebookLoginController animated:YES];
}

- (void)didLoadWebContentVK {
    [self.navigationController pushViewController:self.vkLoginController animated:YES];
}

- (void)reloadMessagesByPullGesture {
    __weak MessagesViewController *weakSelf = self;
    
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.model reloadDataWithCompletion:^{
            [weakSelf.tableView reloadData];
        }];
        [weakSelf.tableView.pullToRefreshView stopAnimating];
    });
    
    
    
    
}


- (void)didRecieveToken:(NSString *)token {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[FacebookService sharedService] loginWithToken:token];
    
    MessagesViewController *vc = [[MessagesViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)didRecieveTokenVK:(NSString *)token andUserId:(NSString*)UserId {
    [self.navigationController popViewControllerAnimated:YES];
    
    [[VKService sharedService] loginWithToken:token andUserId:UserId];
    
    MessagesViewController *vc = [[MessagesViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) recvData:(NSNotification *) notification
{
    [self.model reloadDataWithCompletion:^{
        [self.tableView reloadData];
    }];
    //NSDictionary* userInfo = notification.userInfo;
    //int messageTotal = [[userInfo objectForKey:@"total"] intValue];
    //NSLog (@"Successfully received data from notification! %i", messageTotal);
}


- (void)AddMessage:(id)sender {
    EditeMessageViewController *CController = [[EditeMessageViewController alloc] init];
    
    //CController.delegate = self;
    [self.navigationController pushViewController:CController animated:YES];
   
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.model numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MessagesTableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[MessagesTableViewCell alloc] init];
    }
    [cell setItem:[self.model itemForRow:indexPath.row]];
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    float reload_distance = 10;
    if(y > h + reload_distance) {
        [self.model loadNextPartWithCompletion:^{
            [self.tableView reloadData];
        }];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [MessagesTableViewCell heightForItem:[self.model itemForRow:indexPath.row] widht:tableView.bounds.size.width];
}

#pragma mark - MessagesModelDelegate

- (void)modelDidUpdate {
    [self.tableView reloadData];
}


@end
