//
//  VKService.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//


#import "VKService.h"
#import "Session.h"
#import "Reachability.h"
NSString *VKServiceReachibilityChangedNotification = @"VKServiceReachibilityChangedNotification";



@interface VKService ()

@property (nonatomic, strong) NSURLSession *VKDataSession;
@property (nonatomic, strong) NSURLSession *imagesDownloadSession;

@property (nonatomic, strong) Session *session;

@property (nonatomic, strong) NSMutableDictionary *imagesDowndloadTasks;

@property (nonatomic, strong) Reachability *reachability;


@end

@implementation VKService

+ (instancetype)sharedService {
    static VKService *sharedService = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[VKService alloc] init];
    });
    return sharedService;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _imagesDowndloadTasks = [[NSMutableDictionary alloc] init];
        
        _session = [[Session alloc] init];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
        configuration.timeoutIntervalForRequest = 30.0;
        configuration.timeoutIntervalForResource = 60.0;
        _VKDataSession = [NSURLSession sessionWithConfiguration:configuration];
        
        _reachability = [Reachability reachabilityForInternetConnection];
        [_reachability startNotifier];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChangedNotification:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
    }
    return self;
}

- (void)loginWithToken:(NSString *)token andUserId:(NSString*)UserId {
    self.session.tokenVK = token;
    self.session.UserId = UserId;
   
}

- (BOOL)isLoggedIn {
    return self.session.tokenVK.length > 0;
}

- (void)logoutWithCompletion:(VKServiceCompletion)completion {
    self.session.tokenVK = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *cookiesFolderPath = [libraryPath stringByAppendingString:@"/Cookies"];
        
        NSError *errors;
        
        [[NSFileManager defaultManager] removeItemAtPath:cookiesFolderPath error:&errors];
        
        if (completion) {
            completion(YES, nil);
        }
    });
}
- (void)logOff
{
   [self.session.userDefaults removeObjectForKey:@"SessionTokenUserDefaultsKeyVK"];
    [self.session.userDefaults synchronize];
}

-(void) getUrlwithparams:(NSData*)image andText:(NSString*)text{
   
    NSString* getWallUploadServer = [NSString stringWithFormat:@"https://api.vk.com/method/photos.getWallUploadServer?access_token=%@&v=5.60",self.session.tokenVK];
    NSURL *url = [NSURL URLWithString:getWallUploadServer];
    
    [[self.VKDataSession  dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jsonList = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"jsonList: %@", jsonList);
        
        NSString *upload_url = [[jsonList objectForKey:@"response"] objectForKey:@"upload_url" ];
        [self sendPOSTRequest:upload_url withImageData:image andAccessToken:self.session.tokenVK andUserId:self.session.UserId andText:text];
       
    }] resume];
    
}

- (void) sendPOSTRequest:(NSString *)reqURl withImageData:(NSData *)imageData andAccessToken:(NSString*)accessToken andUserId:(NSString*)userId andText:(NSString*)text{
    NSLog(@"Sending request: %@", reqURl);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    self.VKDataSession  = [NSURLSession sessionWithConfiguration:configuration];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:reqURl]];
    [request setHTTPMethod:@"POST"];
    
    NSString *formBoundary = [NSString stringWithFormat:@"%@.boundary.%08x%08x", @"asdasdasdasd", arc4random(), arc4random()];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", formBoundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"photo\"; filename=\"photo.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"image/jpeg"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:imageData];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", formBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    [[self.VKDataSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *hash = [json objectForKey:@"hash"];
        NSString *photo = [json objectForKey:@"photo"];
        NSString *server = [json objectForKey:@"server"];
        
        NSString* getWallSaveServer = [NSString stringWithFormat:@"https://api.vk.com/method/photos.saveWallPhoto?user_id=%@&access_token=%@&server=%@&photo=%@&hash=%@", userId, accessToken,server,photo,hash];
        
        
        NSString* s1=[getWallSaveServer stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* ppp = [NSURL URLWithString:s1];
        
        
        [[self.VKDataSession  dataTaskWithURL:ppp completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            // NSArray *nsa = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            NSDictionary *jsonList = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            NSLog(@"jsonList: %@", jsonList);
            
            NSDictionary *photoDict = [[jsonList objectForKey:@"response"] lastObject];
            NSString *photoId = [photoDict objectForKey:@"id"];
            [self sendText:accessToken andWithUserId:userId andPhotoId:photoId andText:text];
            
        }] resume];
        
    }] resume];
    
}

-(void) sendText:(NSString*)accessToken andWithUserId:(NSString*)userId andPhotoId:(NSString*)photoId andText:(NSString*)text {
   // NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
     // Создаем запрос на размещение текста на стене
    NSString *sendTextMessage = [NSString stringWithFormat:@"https://api.vk.com/method/wall.post?owner_id=%@&access_token=%@&message=%@&attachment=%@", userId, accessToken,  [self URLEncodedString:text],photoId];
    NSLog(@"sendTextMessage: %@", sendTextMessage);
    
    
    NSURL *url = [NSURL URLWithString:sendTextMessage];
    
    NSURLSessionTask *task = [self.VKDataSession dataTaskWithURL:url
                                        completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                            NSLog(@"get%lu", (unsigned long)json.count);
                                            NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
                                            [userInfo setObject:[NSNumber numberWithInt:1] forKey:@"total"];
                                            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                                            [nc postNotificationName:@"NewMessageArrived" object:self userInfo:userInfo];
                                        }];
    [task resume];
    
   
  
}

- (NSString *)URLEncodedString:(NSString *)str
{
    NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                             (CFStringRef)str,
                                                                                             NULL,
                                                                                             CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                             kCFStringEncodingUTF8));
    
    return result;
}


- (void)loadMediaWithCompletion:(VKServiceDataCompletion)completion {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.vk.com/method/wall.get?count=3&access_token=%@&v=5.60", self.session.tokenVK]];
     //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.vk.com/method/wall.get?access_token=%@&v=5.60", self.session.tokenVK]];
    self.at = self.session.tokenVK;
    [[_VKDataSession dataTaskWithURL:url
                   completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (completion) {
                               completion(!error, data, error);
                           }
                       });
                   }] resume];
}

- (NSString *)loadDataWithURL:(NSString *)urlString completion:(VKServiceDataCompletion)completion {
    NSString *UUID = [NSUUID UUID].UUIDString;
    
    NSURLSessionDataTask *task = [_VKDataSession dataTaskWithURL:[NSURL URLWithString:urlString]
                                               completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       if (completion) {
                                                           completion(!error, data, error);
                                                       }
                                                   });
                                                   [self cancelTaskWithUUID:UUID];
                                               }];
    self.imagesDowndloadTasks[UUID] = task;
    [task resume];
    return UUID;
}

- (void)cancelTaskWithUUID:(NSString *)UUID {
    NSURLSessionDataTask *task = self.imagesDowndloadTasks[UUID];
    [task cancel];
    [self.imagesDowndloadTasks removeObjectForKey:UUID];
}

- (BOOL)online {
    return [self.reachability currentReachabilityStatus] != NotReachable;
}

#pragma mark - Private methods

- (void)reachabilityChangedNotification:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] postNotificationName:VKServiceReachibilityChangedNotification object:nil];
}



@end
