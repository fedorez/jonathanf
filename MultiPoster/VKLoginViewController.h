//
//  VKLoginViewController.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VKLoginViewControllerDelegate <NSObject>

- (void)didLoadWebContentVK;
- (void)didRecieveTokenVK:(NSString *)token andUserId:(NSString*)UserId;

@end
@interface VKLoginViewController : UIViewController
@property (nonatomic, weak) id<VKLoginViewControllerDelegate> delegate;

- (void)loadWebContent;
@end
