 //
//  Mapper.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Mapper.h"

@implementation Mapper
+ (NSArray *)objectsOfClass:(Class<MappingObject>)objectClass
                            withJSONData:(NSData *)data
                            keypath:(NSString *)keypath
                            forSourceType:(NSString *)sourceType
{
    NSError *error = nil;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&error];
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass(self), NSStringFromSelector(_cmd), error);
    }
    NSMutableArray *mutableObjects = [[NSMutableArray alloc] init];
    
    id jsonObject = [JSONDictionary valueForKeyPath:keypath];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        [jsonObject enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [mutableObjects addObject:[self objectOfClass:objectClass withDictionary:obj forSourceType:sourceType]];
        }];
    }
    else {
        [mutableObjects addObject:[self objectOfClass:objectClass withDictionary:jsonObject forSourceType:sourceType]];
    }
    return [mutableObjects copy];
}

+ (NSArray *)entitiesOfClass:(Class<MappingObject>)entityClass
                            withJSONData:(NSData *)data
                            keypath:(NSString *)keypath
                            inContext:(NSManagedObjectContext *)context
                            forSourceType:(NSString *)sourceType{
    NSError *error = nil;
    NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                   options:0
                                                                     error:&error];
    if (error) {
        NSLog(@"%@ %@ %@", NSStringFromClass(self), NSStringFromSelector(_cmd), error);
    }
    NSMutableArray *mutableObjects = [[NSMutableArray alloc] init];
    
    id jsonObject = [JSONDictionary valueForKeyPath:keypath];
    
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        [jsonObject enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [mutableObjects addObject:[self entityOfClass:entityClass withDictionary:obj inContext:context forSourceType:sourceType]];
        }];
    }
    else {
        [mutableObjects addObject:[self entityOfClass:entityClass withDictionary:jsonObject inContext:context forSourceType:sourceType]];
    }
    return [mutableObjects copy];
}

+ (id<MappingObject>)objectOfClass:(Class)objectClass withDictionary:(NSDictionary *)dictionary forSourceType:(NSString *)sourceType {
    NSAssert(![objectClass isSubclassOfClass:[NSManagedObject class]], @"Use entityOfClass:withDictionary:inContext: for creation NSManagedObject subclasses");
    
    NSDictionary *mapping;
    
    if ([sourceType isEqualToString:@"facebook"]) {
        mapping = [objectClass mapping];
    }
    else if ([sourceType isEqualToString:@"vk"]) {
        mapping = [objectClass mappingVK];
    }
    
    id object = [[objectClass alloc] init];
    
    [self setValuesToObject:object fromDictionary:dictionary withMapping:mapping];
    
    return object;
}

+ (id<MappingObject>)entityOfClass:(Class)entityClass withDictionary:(NSDictionary *)dictionary inContext:(NSManagedObjectContext *)context forSourceType:(NSString *)sourceType{
    NSAssert([entityClass isSubclassOfClass:[NSManagedObject class]], @"Use objectOfClass:withDictionary: for creation non NSManagedObject subclasses");
    
    NSString *entityName = nil;
    
    if ([entityClass respondsToSelector:@selector(entityName)]) {
        entityName = [entityClass entityName];
    }
    
    if (!entityName) {
        entityName = NSStringFromClass(entityClass);
    }
    
    NSString *primaryKey = nil;
    
    if ([entityClass respondsToSelector:@selector(primaryKey)]) {
        primaryKey = [entityClass primaryKey];
    }
    
    NSDictionary *mapping;
    
    if ([sourceType isEqualToString:@"facebook"]) {
        mapping = [entityClass mapping];
    }
    else if ([sourceType isEqualToString:@"vk"]) {
        mapping = [entityClass mappingVK];
    }
    __block id entity = nil;
    
    if (primaryKey) {
        __block NSString *primaryKeyMappingKey = nil;
        
        [mapping enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isEqual:primaryKey]) {
                primaryKeyMappingKey = key;
                *stop = YES;
            }
        }];
        
        NSAssert(primaryKeyMappingKey, @"Mapping for primary key not found");
        
        id primaryKeyValue = [dictionary valueForKeyPath:primaryKeyMappingKey];
        NSAssert(primaryKeyValue, @"Primary key value not found");
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@", primaryKey, primaryKeyValue];
        fetchRequest.fetchLimit = 1;
        
        [context performBlockAndWait:^{
            NSError *error = nil;
            NSArray *results = [context executeFetchRequest:fetchRequest error:&error];
            if (error) {
                NSLog(@"%@ %@ %@", NSStringFromClass(self), NSStringFromSelector(_cmd), error);
            }
            if (results.count > 0) {
                entity = results.firstObject;
            }
        }];
    }
    
    if (!entity) {
        entity = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    }
    
    [self setValuesToObject:entity fromDictionary:dictionary withMapping:mapping];
    
    return entity;
}



+ (void)setValuesToObject:(id)object fromDictionary:(NSDictionary *)dictionary withMapping:(NSDictionary *)mapping {
    [mapping enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        id value = [dictionary valueForKeyPath:key];
        NSString *str1 = [NSString stringWithFormat:@"%@",obj];
        NSLog(@"Key %@ === Value %@", obj, value );
        if (!(value == nil) && ![value isEqual:[NSNull null]])
        {
            //if ([value isKindOfClass:[NSArray class]])
            if (([str1 isEqualToString:@"photo_url"]) && ([value isKindOfClass:[NSArray class]]))
            {
                NSDictionary *photos = value[0];
                NSArray *keys = [photos allKeys];
                    NSPredicate *bPredicate =
                    [NSPredicate predicateWithFormat:@"SELF contains[cd] 'photo'"];
                    NSArray *beginWithPhoto =
                    [keys filteredArrayUsingPredicate:bPredicate];
                NSMutableArray *idParts = [[NSMutableArray alloc] init];
                for (int i = 0; i < [beginWithPhoto count]  ; i++)
                {
                     [idParts addObject: [beginWithPhoto[i] componentsSeparatedByString:@"_"][1]];
                }
                NSArray *DescendingArray = [idParts sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                    if ([obj1 integerValue] > [obj2 integerValue]) {
                        return (NSComparisonResult)NSOrderedAscending;
                    }
                    
                    if ([obj1 integerValue] < [obj2 integerValue]) {
                        return (NSComparisonResult)NSOrderedDescending;
                    }
                    return (NSComparisonResult)NSOrderedSame;
                }];
                NSString * pht_url = [@"photo_" stringByAppendingString:(NSString*)DescendingArray[0]];
                    NSLog(@"%@", [photos valueForKey:pht_url]);
                
                 [object setValue:[photos valueForKey:pht_url] forKey:obj];
                }
            
           /* if ([value isKindOfClass:[NSArray class]])
            {
                 [object setValue:value[0] forKey:obj];
            }*/
               else if ([str1 isEqualToString:@"message_dt"])
                { if
                         ([value isKindOfClass:[NSString class]])
                    {
                        NSDateFormatter *df = [[NSDateFormatter alloc] init];
                        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
                        NSDate *dateForItem = [df dateFromString: value];
                        [object setValue:dateForItem forKey:obj];
                    }
                  else{
                      
                      NSDate *date = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
                      [object setValue:date forKey:obj];
                      }
                }
                else
                {
                    [object setValue:(NSString*)value forKey:obj];
                }
         }
        // if (value && ![value isEqual:[NSNull null]]) {
            /*if ([str1 isEqualToString:@"photo_url"])
                if ([value isKindOfClass:[NSArray class]]) {
                    [object setValue:value[0] forKey:obj];
                }
                else{
                    [object setValue:value forKey:obj];
                }
            else if ([str1 isEqualToString:@"message_dt"]) {
                    NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:(NSInteger)value];
                    [object setValue:date forKey:obj];
             
                }
                else{
                [object setValue:value forKey:obj];}*/
        //}
    }];
}

@end
