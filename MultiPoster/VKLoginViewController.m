//
//  VKLoginViewController.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "VKLoginViewController.h"
#import <WebKit/WebKit.h>


@interface VKLoginViewController ()<WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic ,strong) WKNavigation *loginNavigation;

@end

@implementation VKLoginViewController

- (void)loadView {
    self.view = self.webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (navigation == self.loginNavigation) {
        if (self.delegate) {
            [self.delegate didLoadWebContentVK];
        }
    }
}


- (void)loadWebContent {
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero
                                      configuration:[[WKWebViewConfiguration alloc] init]];
    self.webView.navigationDelegate = self;
    self.webView.opaque = NO;
    
    NSURL *url = [NSURL URLWithString:@"https://oauth.vk.com/authorize?client_id=5778453&display=mobile&redirect_uri=https://oauth.vk.com/blank.html/callback&scope=friends,photos,wall&response_type=token&v=5.60&state=123456"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loginNavigation = [self.webView loadRequest:request];
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    NSMutableDictionary* user = [[NSMutableDictionary alloc] init];
    NSURL *url = [webView URL];
    NSString *currentURL = [url absoluteString];
    NSRange textRange =[[currentURL lowercaseString] rangeOfString:[@"access_token" lowercaseString]];
    
    if(textRange.location != NSNotFound){
        
        NSArray* data = [currentURL componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];
        [user setObject:[data objectAtIndex:1] forKey:@"access_token"];
        [user setObject:[data objectAtIndex:3] forKey:@"expires_in"];
        [user setObject:[data objectAtIndex:5] forKey:@"user_id"];
        
        
        NSString *token = [user objectForKey:@"access_token"];
        NSString *user_id = [user objectForKey:@"user_id"];
        //self.u = [self.user objectForKey:@"user_id"];
        [self.delegate didRecieveTokenVK: token andUserId:user_id];
        
        
    }
    else {
        //Ну иначе сообщаем об ошибке...
        textRange =[[currentURL lowercaseString] rangeOfString:[@"access_denied" lowercaseString]];
        if (textRange.location != NSNotFound) {
            
        }
    }}




@end
