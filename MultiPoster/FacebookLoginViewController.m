//
//  FacebookLoginViewController.m
//  MultiPoster
//
//  Created by Denis Fedorets on 20.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "FacebookLoginViewController.h"
#import <WebKit/WebKit.h>



@interface FacebookLoginViewController () <WKNavigationDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic ,strong) WKNavigation *loginNavigation;

@end

@implementation FacebookLoginViewController

- (void)loadView {
    self.view = self.webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (navigation == self.loginNavigation) {
        if (self.delegate) {
            [self.delegate didLoadWebContent];
        }
    }
}

- (void)loadWebConttent {
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero
                                      configuration:[[WKWebViewConfiguration alloc] init]];
    self.webView.navigationDelegate = self;
    self.webView.opaque = NO;
    
    NSURL *url = [NSURL URLWithString:@"http://www.facebook.com/dialog/oauth/?client_id=1873897766179838&redirect_uri=http://nonactivesite.no/&scope=email,user_posts,user_photos,manage_pages,publish_pages,publish_actions"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    self.loginNavigation = [self.webView loadRequest:request];
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    NSURL *url = [webView URL];
    NSString *absoluteString = [url absoluteString];
    if ([absoluteString hasPrefix:@"http://nonactivesite.no"]) {
        if (self.delegate) {
            NSString *code = [self getFacebookQueryFromRedirectURL: url];
            NSLog(@"%@", code);
            
            NSString *tokenUriString = [NSString stringWithFormat:@"%@%@%@%@", @"https://graph.facebook.com/v2.7/oauth/access_token?code=", code, @"&client_secret=b792a8613e7d3489e134f84c337042dd", @"&client_id=1873897766179838&redirect_uri=http://nonactivesite.no/"];
            
            NSURL *urlToObtainToken = [NSURL URLWithString: tokenUriString];
            NSURLRequest *requestToObtainToken = [NSURLRequest requestWithURL:urlToObtainToken];
            //магия фейбука работает именно в таком виде, строкой. [NSMutableURLRequest requestWithURL] а потом установить параметры через setValue: forHTTPHeaderField: - не работает, странная внутренняя ошибка FB, о чём плачет stackoverflow уже больше года
            NSData *responseToObtainToken = [NSURLConnection sendSynchronousRequest:requestToObtainToken returningResponse:nil error:nil];
            
            NSError *error = nil;
            
            NSDictionary *dataTokenDictionary = [NSJSONSerialization JSONObjectWithData:responseToObtainToken
                                                                                options:0
                                                                                  error:&error];
            if(!error){
                
                NSString *token = dataTokenDictionary[@"access_token"];
                [self.delegate didRecieveToken: token];
            }
        }
    }
}

- (NSString*) getFacebookQueryFromRedirectURL: (NSURL *) url {
    NSString *code = [[[url query]componentsSeparatedByString:@"="] objectAtIndex:1];
    return code;
}

@end
