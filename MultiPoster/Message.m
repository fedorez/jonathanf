//
//  Message.m
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Message.h"

@implementation Message

+ (NSDictionary *)mapping {
    return @{
             @"full_picture" : @"photo_url",
             @"likes.summary.total_count" : @"likesCount",
             @"id" : @"messageIdString",
             @"message" : @"text",
             @"updated_time" : @"message_dt"
             };
}


+ (NSDictionary *)mappingVK {
    return @{
             @"attachments.photo" : @"photo_url",
             @"likes.count" : @"likesCount",
             @"id" : @"messageIdString",
             @"text" : @"text",             
             @"date" : @"message_dt"
             };
}

+ (NSString *)primaryKey {
    return @"messageIdString";
}

- (void)setMessageIdString:(id)messageIdString {
    [self willChangeValueForKey:@"messageIdString"];
    
    NSString *str1 = [NSString stringWithFormat:@"%@",messageIdString];
    
  //  NSString *messageIdStringStr = (NSString *)messageIdString;
    [self setPrimitiveValue:str1 forKey:@"messageIdString"];
   
    //NSArray *idParts = [messageIdString componentsSeparatedByString:@","];
    //NSAssert(idParts.count == 2, @"Wrong id pattern");
    
    self.messageId = @([messageIdString integerValue]);
    
    [self didChangeValueForKey:@"messageIdString"];
}


@end
