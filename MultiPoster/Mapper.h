//
//  Mapper.h
//  MultiPoster
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MappingObject.h"

typedef void (^MapperCompletion) (NSArray *objects);


@interface Mapper : NSObject

+ (NSArray *)objectsOfClass:(Class<MappingObject>)objectClass
                            withJSONData:(NSData *)data
                            keypath:(NSString *)keypath
                            forSourceType:(NSString *)sourceType;

+ (NSArray *)entitiesOfClass:(Class<MappingObject>)entityClass
                            withJSONData:(NSData *)data
                            keypath:(NSString *)keypath
                            inContext:(NSManagedObjectContext *)context
                            forSourceType:(NSString *)sourceType;
@end
